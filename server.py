
from gevent import socket
from gevent.pool import Pool
from gevent.server import StreamServer

from collections import namedtuple
from io import BytesIO
import logging

logger = logging.getLogger(__name__)


class CommandError(Exception): pass
class Disconnect(Exception): pass


Error = namedtuple('Error', ('message',))


class ProtocolHandler(object):
    def __init__(self):
        self.handlers = {
            '+': self.handle_simple_string,
            '-': self.handle_error,
            ':': self.handle_integer,
            '$': self.handle_string,
            '*': self.handle_array,
            '%': self.handle_dict}

    def handle_request(self, socket_file):
        first_byte = socket_file.read(1).decode('utf-8')
        if not first_byte:
            raise Disconnect()
        try:
            return self.handlers[first_byte](socket_file)
        except KeyError:
            raise CommandError('bad request')

    def handle_simple_string(self, socket_file):
        return socket_file.readline().decode('utf-8').rstrip('\r\n')

    def handle_error(self, socket_file):
        return Error(socket_file.readline().decode('utf-8').rstrip('\r\n'))

    def handle_integer(self, socket_file):
        return int(socket_file.readline().decode('utf-8').rstrip('\r\n'))

    def handle_string(self, socket_file):
        data = socket_file.readline().decode('utf-8')
        length = int(data.rstrip('\r\n'))
        if length == -1:
            return None  # Special-case for NULLs.
        length += 2  # Include the trailing \r\n in count.
        return socket_file.read(length)[:-2]

    def handle_array(self, socket_file):
        num_elements = int(socket_file.readline().decode('utf-8').rstrip('\r\n'))
        return [self.handle_request(socket_file) for _ in range(num_elements)]

    def handle_dict(self, socket_file):
        num_items = int(socket_file.readline().decode('utf-8').rstrip('\r\n'))
        elements = [self.handle_request(socket_file)
                    for _ in range(num_items * 2)]
        return dict(zip(elements[::2], elements[1::2]))

    def write_response(self, socket_file, data):
        buf = BytesIO()
        if isinstance(data, (list, tuple)) and len(data) == 1:
            data = data[0]
        self._write(buf, data)
        buf.seek(0)
        socket_file.write(buf.getvalue())
        socket_file.flush()

    def _write(self, buf, data):
        if isinstance(data, bytes):
            data = data.decode('utf-8')
            cmd = '$%s\r\n%s\r\n' % (len(data), data)
            buf.write(cmd.encode('utf-8'))
        if isinstance(data, str):
            cmd = '$%s\r\n%s\r\n' % (len(data), data)
            buf.write(cmd.encode('utf-8'))
        elif isinstance(data, int):
            cmd = ':%s\r\n' % data
            buf.write(cmd.encode('utf-8'))
        elif isinstance(data, Error):
            cmd = '-%s\r\n' % Error.message
            buf.write(cmd.encode('utf-8'))
        elif isinstance(data, (list, tuple)):
            cmd = '*%s\r\n' % len(data)
            buf.write(cmd.encode('utf-8'))
            for item in data:
                self._write(buf, item)
        elif isinstance(data, dict):
            print('data: ', data)
            cmd = '%%%s\r\n' % len(data)
            buf.write(cmd.encode('utf-8'))
            for key in data:
                self._write(buf, key)
                self._write(buf, data[key])
        elif data is None:
            buf.write('$-1\r\n'.encode('utf-8'))
        else:
            raise CommandError('unrecognized type: %s' % type(data))


class Server(object):
    def __init__(self, host='127.0.0.1', port=31337, max_clients=64):
        self._pool = Pool(max_clients)
        self._server = StreamServer(
            (host, port),
            self.connection_handler,
            spawn=self._pool)

        self._protocol = ProtocolHandler()
        self._kv = {}

        self._commands = self.get_commands()

    def get_commands(self):
        return {
            'GET': self.get,
            'SET': self.set,
            'DELETE': self.delete,
            'FLUSH': self.flush,
            'MGET': self.mget,
            'MSET': self.mset,
            'GETALL': self.getall}

    def connection_handler(self, conn, address):
        logger.info('Connection received: %s:%s' % address)
        socket_file = conn.makefile('rwb')

        while True:
            try:
                data = self._protocol.handle_request(socket_file)
            except Disconnect:
                logger.info('Client went away: %s:%s' % address)
                break

            try:
                resp = self.get_response(data)
            except CommandError as exc:
                logger.exception('Command error')
                resp = Error(exc.args[0])

            self._protocol.write_response(socket_file, resp)

    def run(self):
        self._server.serve_forever()

    def get_response(self, data):
        if not isinstance(data, list):
            try:
                data = data.split()
            except AttributeError:
                raise CommandError('Request must be list or simple string.')
        data = [item.decode('utf-8')
                if not isinstance(item, (list, tuple, dict, int))
                else item for item in data]
        if not data:
            raise CommandError('Missing command')

        command = data[0].upper()
        if command not in self._commands:
            raise CommandError('Unrecognized command: %s' % command)
        else:
            logger.debug('Received %s', command)

        return self._commands[command](*data[1:])

    def get(self, key):
        return self._kv.get(key)

    def getall(self):
        return " ".join([str(x) for x in self._kv.items()])

    def set(self, key, value):
        self._kv[key] = value
        return 1

    def delete(self, key):
        if key in self._kv:
            del self._kv[key]
            return 1
        return 0

    def flush(self):
        kvlen = len(self._kv)
        self._kv.clear()
        return kvlen

    def mget(self, *keys):
        return [self._kv.get(key) for key in keys]

    def mset(self, *items):
        data = zip(items[::2], items[1::2])
        for key, value in data:
            self._kv[key] = value
        return len(items)//2


class Client(object):
    def __init__(self, host: str ='127.0.0.1', port=31337):
        self._protocol = ProtocolHandler()
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.connect((host, port))
        self._fh = self._socket.makefile('rwb')
        logger.debug(f"{type(self._fh)}")

    def execute(self, *args):
        self._protocol.write_response(self._fh, args)
        resp = self._protocol.handle_request(self._fh)
        if isinstance(resp, Error):
            raise CommandError(resp.message)
        return resp

    def get(self, key):
        return self.execute('GET', key)

    def set(self, key, value):
        return self.execute('SET', key, value)

    def delete(self, key):
        return self.execute('DELETE', key)

    def flush(self):
        return self.execute('FLUSH')

    def mget(self, *keys):
        return self.execute('MGET', *keys)

    def mset(self, *items):
        return self.execute('MSET', *items)

    def getall(self):
        return self.execute('GETALL')


if __name__ == '__main__':
    from gevent import monkey;
    import sys

    monkey.patch_all()
    logger.addHandler(logging.StreamHandler(stream=sys.stdout))
    logger.setLevel(logging.DEBUG)
    Server().run()



# def _write(self, buf, data):
#     #     if isinstance(data, str):
#     #         data = data.encode('utf-8')
#     #     print(type(data))
#     #     match data:
#     #         case bytes():
#     #             buf.write(b'$%s\r\n%s\r\n' % (len(data), data))
#     #         case int():
#     #             buf.write(b':%s\r\n' % data)
#     #         case Error():
#     #             buf.write(b'-%s\r\n' % Error.message)
#     #         case list() | tuple():
#     #             buf.write(b'*%s\r\n' % len(data))
#     #             for item in data:
#     #                 self._write(buf, item)
#     #         case dict():
#     #             if data:
#     #                 buf.write(b'%%%s\r\n' % len(data))
#     #                 for key in data:
#     #                     self._write(buf, key)
#     #                     self._write(buf, data[key])
#     #             elif data is None:
#     #                 buf.write(b'$-1\r\n')
#     #         case _:
#     #             raise CommandError('unrecognized type: %s' % type(data))
